#!/bin/bash -e
clear
echo "============================================"
echo "WordPress Install Script"
echo "============================================"
echo "Database Name: "
read dbname
echo "Database User: "
read dbuser
echo "Database Password: "
read dbpass
echo "run install? (y/n): "
read run

if [ $run == n ]; then
  exit
else
  echo "============================================"
  echo "A robot is now installing WordPress for you."
  echo "============================================"
  #download wordpress
  FILENAME_WP="latest-es_ES.tar.gz"
  curl -O https://es.wordpress.org/$FILENAME_WP
  #unzip wordpress
  tar -zxvf $FILENAME_WP
  #change dir to wordpress
  cd wordpress
  #copy file to parent dir
  cp -rf . ..
  #move back to parent dir
  cd ..
  #remove files from wordpress folder
  rm -R wordpress
  #create wp config
  cp wp-config-sample.php wp-config.php
  #set database details with perl find and replace
  perl -pi -e "s/database_name_here/$dbname/g" wp-config.php
  perl -pi -e "s/username_here/$dbuser/g" wp-config.php
  perl -pi -e "s/password_here/$dbpass/g" wp-config.php

  #set WP salts
  perl -i -pe'
    BEGIN {
      @chars = ("a" .. "z", "A" .. "Z", 0 .. 9);
      push @chars, split //, "!@#$%^&*()-_ []{}<>~\`+=,.;:/?|";
      sub salt { join "", map $chars[ rand @chars ], 1 .. 64 }
    }
    s/put your unique phrase here/salt()/ge
  ' wp-config.php

  #create uploads folder and set permissions
  mkdir wp-content/uploads
  chmod 775 wp-content/uploads
  echo "Cleaning..."
  #remove zip file
  rm $FILENAME_WP

  echo "========================="
  echo "Installation is complete."
  echo "========================="
fi